USE ModernWays;
SELECT DISTINCT Studenten_Id AS 'Id'
FROM Evaluaties
GROUP BY Studenten_Id
HAVING AVG(Cijfer) > (SELECT AVG(Cijfer) 
FROM Evaluaties WHERE Cijfer);