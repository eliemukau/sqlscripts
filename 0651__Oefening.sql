USE `aptunes`;

DELIMITER $$
CREATE PROCEDURE GetAlbumDuration(IN album INT, OUT totalDuration SMALLINT UNSIGNED)

BEGIN
	DECLARE songDuration TINYINT UNSIGNED DEFAULT 0;
    DECLARE ok BOOL DEFAULT FALSE;
    DECLARE songDurationCursor CURSOR FOR SELECT Lengte FROM Liedjes WHERE Albums_ID;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET ok = TRUE;
    
    SET totalDuration = 0;
    OPEN songDurationCursor;
    fetchloop: LOOP
    fetch songDurationCursor INTO songDuration;
    IF ok = TRUE THEN
		LEAVE fetchloop;
	END IF;
    SET totalDuration = totalDuration + songDuration;
	END LOOP;
    CLOSE songDurationCursor;
END $$
DELIMITER ;