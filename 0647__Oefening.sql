USE `aptunes`;
DROP PROCEDURE IF EXISTS MockAlbumReleasesLoop;

DELIMITER $$
CREATE PROCEDURE MockAlbumReleasesLoop(IN extraReleases INT)
BEGIN
DECLARE counter INT DEFAULT 0;
DECLARE success BOOL;
CALLLOOP:LOOP
	CALL MockAlbumReleaseWithSuccess(success);
    IF succes = 1 THEN
    SET counter = counter + 1;
    END IF;
	IF counter = extraReleases THEN
    LEAVE CALLLOOP;
    END IF;
    END LOOP;
END$$
DELIMITER ;