USE ModernWays;

SELECT Voornaam FROM Studenten
WHERE Voornaam IN (SELECT DISTINCT Voornaam 
FROM Personeelsleden) AND Voornaam IN (SELECT DISTINCT Voornaam FROM Directieleden);