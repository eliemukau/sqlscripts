USE aptunes;
DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `NumberOfGenres` (OUT totalGenres TINYINT)
BEGIN
	SELECT COUNT(*)
    INTO totalGenres
    FROM Genres;
END$$
DELIMITER ;