USE `aptunes`;
DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CleanupOldMemberships`(IN thisDate DATE, OUT numberCleaned INT)
BEGIN
	START TRANSACTION;
	SELECT COUNT(*)
    INTO numberCleaned
    FROM Lidmaatschappen
    WHERE Einddatum IS NOT NULL AND Lidmaatscappen.Einddatum < thisDate;
    
	SET sql_safe_updates = 0;
	DELETE FROM Lidmaatschappen
	WHERE Lidmaatscappen.Einddatum IS NOT NULL AND Lidmaatscappen.Einddatum < thisDate;
    DELETE
    FROM Lidmaatschappen
    WHERE Lidmaatscappen.Einddatum IS NOT NULL AND Lidmaatschappen.Einddatum < thisDate;
	SET sql_safe_updates = 1;
	COMMIT;
END$$
DELIMITER ;