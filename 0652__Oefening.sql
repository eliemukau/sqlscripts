USE `aptunes`;

CREATE USER IF NOT EXISTS student IDENTIFIED BY 'ikbeneenstudent';
GRANT EXECUTE ON PROCEDURE GetAlbumDuration TO student;
GRANT EXECUTE ON PROCEDURE GetAlbumDuration2 TO student;

