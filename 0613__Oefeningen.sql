USE ModernWays;
select concat(Personen.Familienaam, ' ', Personen.Voornaam) as 'Auteur', Boeken.Titel from Publicaties
inner join Boeken on Boeken.Id = Publicaties.Boeken_Id
inner join Personen on Personen.Id = Publicaties.Personen_Id;