CREATE INDEX idx_TitelLengte
ON Liedjes(Titel, Lengte);

SELECT Titel, Naam, Lengte FROM Liedjes
INNER JOIN Bands
ON Liedjes.Bands_Id = Bands.Id
WHERE Titel LIKE 'A%'
ORDER BY Lengte;