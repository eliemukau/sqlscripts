USE `aptunes`;
DELIMITER $$
CREATE PROCEDURE `CreateAndReleaseAlbum`(IN titel VARCHAR(100),IN bands_Id INT)
BEGIN
	INSERT INTO Albums (Titel)
	VALUES (titel);
	INSERT INTO Albumreleases (Bands_id, Albums_id)
	VALUES(Bands_Id, LAST_INSERT_ID());
	COMMIT;
	END$$
DELIMITER ;