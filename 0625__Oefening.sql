CREATE INDEX idx_FamilienaamVoornaam
ON Muzikanten(Familienaam, Voornaam);

SELECT Voornaam, Familienaam, COUNT(Lidmaatschappen.Muzikanten_Id)
FROM Muzikanten RIGHT JOIN Lidmaatschappen
ON Lidmaatschappen.Muzikanten_Id = Muzikanten.Id
GROUP BY Familienaam, Voornaam
ORDER BY Voornaam, Familienaam desc;