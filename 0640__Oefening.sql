USE `aptunes`;
DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetLiedjes`(IN core VARCHAR(50))
BEGIN
	SELECT Titel
	FROM albums
	WHERE Titel LIKE CONCAT('%', core, '%');
END$$
DELIMITER ;