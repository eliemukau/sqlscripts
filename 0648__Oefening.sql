USE`aptunes`;
DELIMITER $$

CREATE PROCEDURE DemonstrateHandlerOrder()
BEGIN
	DECLARE randomValue  tinyint default 0;
    DECLARE CONTINUE HANDLER FOR SQLSTATE '45002'
BEGIN
	SELECT 'STATE 45002 opgevangen. Geen probleem.';
END;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
BEGIN
	SELECT 'Een algemene fout opgevangen';
END;
    SET randomValue = floor(rand() * 3) +1;
    IF randomValue = 1 THEN
		SIGNAL  SQLSTATE '45001';
	ELSEIF randomValue = 2 THEN
		SIGNAL SQLSTATE '45002';
	ELSE
		SIGNAL SQLSTATE '45003';
	END IF;
END $$

DELIMITER $$