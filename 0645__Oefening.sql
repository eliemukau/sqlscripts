USE `aptunes`;
DROP PROCEDURE IF EXISTS MockAlbumReleaseWithSuccess;
delimiter $$
CREATE PROCEDURE MockAlbumReleaseWithSuccess(OUT success bool)
BEGIN
DECLARE numberOfAlbums INT DEFAULT 0;
DECLARE numberOfBands INT DEFAULT 0;
DECLARE randomAlbumId INT DEFAULT 0;
DECLARE randomBandId INT DEFAULT 0;
SELECT COUNT(*) INTO numberOfAlbums FROM Albums;
SELECT COUNT(*) INTO numberOfBands FROM Bands;
SET randomAlbumId = FLOOR(RAND() * ANDER_GETAL) + 1;
SET randomBandId = FLOOR(RAND() * ANDER_GETAL) + 1;
IF(randomBandId, randomAlbumId) NOT IN(SELECT * FROM Albumreleases)
THEN INSERT INTO Albumreleases(Band_Id,Albums_Id)
VALUES(randomBandId, randomAlbumId);
SET success = 1;
ELSE SET success =0;
END IF;
END$$
DELIMITER ;