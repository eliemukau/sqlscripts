Use `aptunes`;
DROP PROCEDURE IF EXISTS DangerousInsertAlbumReleases;

DELIMITER $$
CREATE PROCEDURE DangerousInsertAlbumReleases()
BEGIN
	DECLARE numberOfAlbums int default 0;
    DECLARE numberOfBands int default 0;
	DECLARE randomAlbumId1 int default 0;
    DECLARE randomBandId1 int default 0;
	DECLARE randomAlbumId2 int default 0;
	DECLARE randomBandId2 int default 0;
    DECLARE randomBandId3 int default 0;
	DECLARE randomAlbumId3 int default 0;
    DECLARE randomValue TINYINT DEFAULT 0;
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		ROLLBACK;
        SELECT 'Nieuwe releases konden niet worden toegevoegd';
	END;
	SELECT COUNT(*) INTO  numberOfAlbums FROM Albums;
    SELECT COUNT(*) INTO numberOfBands FROM Bands;
    SET randomAlbumId1 = FLOOR(RAND() * numberOfAmbums) + 1;
	SET randomAlbumId2 = FLOOR(RAND() * numberOfAmbums) + 1;
	SET randomAlbumId3 = FLOOR(RAND() * numberOfAmbums) + 1;
	SET randomBandId1 = FLOOR(RAND() * numberOfBands) + 1;
	SET randomBandId2 = FLOOR(RAND() * numberOfBands) + 1;
	SET randomBandId3 = FLOOR(RAND() * numberOfBands) + 1;
    
    START TRANSACTION;
    
    INSERT INTO Albumreleases(Bands_Id,Albums_Id)
    VALUES
    (randomBandId1, randomAlbumId1),
	(randomBandId2, randomAlbumId2);
    SET randomValue = floor(rand() * 3) + 1;
    
    IF randomValue = 1 THEN
		SIGNAL SQLSTATE '45000';
	END IF;
    INSERT INTO Albumreleases(Bands_Id,Albums_Id)
    VALUES
	(randomBandId3, randomAlbumId3);
	
    COMMIT;
END $$
DELIMITER $$