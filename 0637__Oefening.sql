-- creer een nieuwe tabel met de auteursgegevens
-- Voornaam en Familienaam
use ModernWays;
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);
-- gegevens uit de tabel Boeken overzetten naar de tabel Personen
-- we gebruiken hiervoor een subquery
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;
-- er zijn geen dubbele waarden meer
-- tabel personen uitbreiden met extra informatie over de personen
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);
   -- elke auteur komt 1x voor en heeft een unieke ID
  -- de waarde van de Personen_Id is 0 na het uitvoeren van een select

-- foreign key verplicht maken
-- verwijzing zou beter not null zijn
-- op dit moment niet mogelijk, want de kolom moet ingevuld worden
-- op basis van vergelijking tussen Boeken en Personen
alter table Boeken add Personen_Id int null;
-- kopieer in de geldige combinaties Boeken / Personen
-- het ID van de persoon
-- naar de verwijzing in een Boek naar een Persoon
-- m.a.w. zorg ervoor dat een Boek verwijst naar zijn auteur
set sql_safe_updates=0;
update Boeken cross join Personen
    set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
set sql_safe_updates=1;
-- nu mogen ID's verplicht worden
-- foreign key verplicht maken
alter table Boeken change Personen_Id Personen_Id int not null;

alter table Boeken drop column Voornaam,
    drop column Familienaam;
    -- dan de constraint toevoegen
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);
    
  
