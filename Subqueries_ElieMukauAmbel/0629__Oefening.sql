USE ModernWays;

SELECT Studenten_Id FROM Evaluaties
INNER JOIN STUDENTEN ON
Studenten.Id = Studenten_Id
WHERE Cijfer >=(SELECT AVG(Cijfer) 
FROM Evaluaties WHERE Cijfer);